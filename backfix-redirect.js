!(function () {
    const depth = 2,
      getUrl = function () {
        return '<?=$showcaseUrl?>';
      },
      replaceMacros = function (backLink) {
        const DefaultMacroValue = '',
          regExp = /\[(.+?)]/g;
  
        try {
          var url = new URL(location.href);
  
          backLink = backLink.replace(regExp, function (all, key) {
            if (url.searchParams.has(key)) {
              return url.searchParams.get(key);
            }
            return DefaultMacroValue;
          });
  
          return backLink;
        } catch (err) {
          console.error(err);
          return backLink;
        }
      };
  
    var disabled = false;
  
    try {
      if (disabled === true) {
        (window.parent || window).history.go(-depth);
        return;
      }
  
      const URL = window.location.href.split(/[#]/)[0];
      for (let t = 0; depth > t; ++t) (window.parent || window).history.pushState({}, '', URL + '#' + t);
  
      (window.parent || window).onpopstate = (event) => {
        if (disabled === false) {
          event.state && location.replace(replaceMacros(getUrl()));
        }
      };
    } catch (err) {
      console.log(err);
    }
  })();